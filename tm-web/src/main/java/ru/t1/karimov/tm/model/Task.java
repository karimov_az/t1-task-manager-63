package ru.t1.karimov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Task {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    private String name;

    @Nullable
    private String description;

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @Nullable
    private String projectId;

    public Task(@NotNull final String name) {
        this.name = name;
    }

}
